/*
 * Author: Raffaele GAETANO
 *
 * Affiliation: CIRAD, UMR TETIS
 */

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbLabelImageToOGRDataSourceFilter.h"
#include "otbOGRFeatureWrapper.h"

namespace otb
{
namespace Wrapper
{
class SimpleVectorization : public Application
{
public:
  typedef SimpleVectorization             Self;
  typedef Application                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  typedef UInt32ImageType                   LabelImageType;

  typedef otb::LabelImageToOGRDataSourceFilter<LabelImageType> LabelImageToOGRDataSourceFilterType;

  itkNewMacro(Self);

  itkTypeMacro(Vectorization, otb::Application);

private:
  void DoInit() override
  {
    SetName("SimpleVectorization");
    SetDescription("This application performs the simple vectorization of a label image (ex. from segmentation).");

    SetDocLongDescription(
        "Given a segmentation result (label image), it will convert"
        " it to a GIS vector file containing one polygon per segment.");
    SetDocLimitations("This application is proposed as part of the LSGRM Remote Module from Remi Cresson / Pierre Lassalle.");
    SetDocAuthors("Raffaele Gaetano");

    AddDocTag(Tags::Segmentation);
    AddDocTag("LSGRM");

    AddParameter(ParameterType_InputImage, "in", "Input Segmentation Raster");
    SetParameterDescription("in", "The input label image.");
    
    AddParameter(ParameterType_InputImage, "mask", "Input mask");
    SetParameterDescription("mask",
                            "Optional mask to indicate which pixels are valid for vectorization.");
    MandatoryOff("mask");
    DisableParameter("mask");
    
    AddParameter(ParameterType_Int, "bv", "Background Value");
    SetParameterDescription("bv", "Value corresponding to no-object.");
    SetDefaultParameterInt("bv", 0);

    AddParameter(ParameterType_OutputFilename, "out", "Output GIS vector file");
    SetParameterDescription("out",
                            "The output GIS vector file, representing the vectorized version of the label image.");

    AddRAMParameter();

    // Doc example parameter settings
    SetDocExampleParameterValue("in", "seg.tif");
    SetDocExampleParameterValue("out", "seg.shp");

  }

  void DoUpdateParameters() override
  {
  }

  void DoExecute() override
  {
    std::string shapefile(GetParameterString("out"));

    LabelImageType::Pointer labelIn = GetParameterUInt32Image("in");
    labelIn->UpdateOutputInformation();
    std::string   projRef                    = labelIn->GetProjectionRef();

	// Raster->Vecteur conversion
	otb::ogr::DataSource::Pointer ogrDS;
	otb::ogr::Layer               layer(nullptr, false);
	OGRSpatialReference      oSRS(projRef.c_str());
	std::vector<std::string> options;
	
	ogrDS                 = otb::ogr::DataSource::New(shapefile, otb::ogr::DataSource::Modes::Overwrite);
	std::string layername = itksys::SystemTools::GetFilenameName(shapefile);
	std::string extension = itksys::SystemTools::GetFilenameLastExtension(shapefile);
	layername             = layername.substr(0, layername.size() - (extension.size()));
	layer                 = ogrDS->CreateLayer(layername, &oSRS, wkbPolygon, options);
	
	OGRFieldDefn labelField("label", OFTInteger);
	layer.CreateField(labelField, true);

	LabelImageToOGRDataSourceFilterType::Pointer labelToOGR = LabelImageToOGRDataSourceFilterType::New();
	labelToOGR->SetInput(labelIn);
	
	if (IsParameterEnabled("mask")) {
	  LabelImageType::Pointer        mask             = this->GetParameterUInt32Image("mask");
	  labelToOGR->SetInputMask(mask);
	}
	labelToOGR->SetFieldName("label");
	labelToOGR->Update();
	
	int noObj = this->GetParameterInt("bv");
	otb::ogr::DataSource::ConstPointer ogrDSTmp = labelToOGR->GetOutput();
	otb::ogr::Layer                    layerTmp = ogrDSTmp->GetLayerChecked(0);
	otb::ogr::Layer::const_iterator featIt = layerTmp.begin();
	for (; featIt != layerTmp.end(); ++featIt)
	{
	  if (featIt->ogr().GetFieldAsInteger("label") != noObj) {
		otb::ogr::Feature dstFeature(layer.GetLayerDefn());
		dstFeature.SetFrom(*featIt, TRUE);
		layer.CreateFeature(dstFeature);
	  }
	}
	ogrDS->SyncToDisk();
  }

};
}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::SimpleVectorization)
