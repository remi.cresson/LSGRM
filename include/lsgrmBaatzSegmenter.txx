#ifndef __LSGRM_BAATZ_SEGMENTER_TXX
#define __LSGRM_BAATZ_SEGMENTER_TXX
#include "lsgrmBaatzSegmenter.h"
namespace lsgrm
{

template<class TImage>
void
BaatzSegmenter<TImage>::WriteSpecificAttributes(NodePointerType node, FILE * nodeStream)
{
  std::size_t bands = node->m_Means.size();
  fwrite(&(bands), sizeof(bands), 1, nodeStream);

  for(unsigned int b = 0; b < node->m_Means.size(); b++)
    {
    fwrite(&(node->m_Means[b]), sizeof(node->m_Means[b]), 1, nodeStream);
    fwrite(&(node->m_SquareMeans[b]), sizeof(node->m_SquareMeans[b]), 1, nodeStream);
    fwrite(&(node->m_SpectralSum[b]), sizeof(node->m_SpectralSum[b]), 1, nodeStream);
    fwrite(&(node->m_Std[b]), sizeof(node->m_Std[b]), 1, nodeStream);
    }
}

template<class TImage>
void
BaatzSegmenter<TImage>::ReadSpecificAttributes(NodePointerType node, FILE * nodeStream)
{
  std::size_t bands;
  fread(&(bands), sizeof(bands), 1, nodeStream);
  node->m_Means.assign(bands, 0);
  node->m_SquareMeans.assign(bands, 0);
  node->m_SpectralSum.assign(bands, 0);
  node->m_Std.assign(bands, 0);

  for(unsigned int b = 0; b < bands; b++)
    {
    fread(&(node->m_Means[b]), sizeof(node->m_Means[b]), 1, nodeStream);
    fread(&(node->m_SquareMeans[b]), sizeof(node->m_SquareMeans[b]), 1, nodeStream);
    fread(&(node->m_SpectralSum[b]), sizeof(node->m_SpectralSum[b]), 1, nodeStream);
    fread(&(node->m_Std[b]), sizeof(node->m_Std[b]), 1, nodeStream);
    }
}

template<class TImage>
long long unsigned int
BaatzSegmenter<TImage>::GetSpecificAttributesMemory(NodePointerType &node)
{
  long long unsigned int memory = 0;

  memory += 4 * node->m_Means.size() * sizeof(float); // 4 vectors values
  memory += 4 * sizeof(std::vector<float>);           // 4 vector containers

  return memory;
}
} // end of namespace lsrm

#endif







