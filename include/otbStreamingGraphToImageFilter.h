/*
 * otbStreamingGraphToImageFilter.h
 *
 *  Created on: 6 nov. 2017
 *      Author: cresson
 */

#ifndef MODULES_REMOTE_LSGRM_INCLUDE_OTBSTREAMINGGRAPHTOIMAGEFILTER_H_
#define MODULES_REMOTE_LSGRM_INCLUDE_OTBSTREAMINGGRAPHTOIMAGEFILTER_H_

#include "itkImageSource.h"
#include "itkExceptionObject.h"
#include "itkImageRegion.h"
#include "itkGrayscaleFillholeImageFilter.h"

#include "grmGraphOperations.h"

// Boost R-Tree
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>

#include <boost/geometry/index/rtree.hpp>

// to store queries results
#include <vector>

// just for output
#include <iostream>
#include <boost/foreach.hpp>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

namespace lsgrm
{

template <class TGraph, class TLabelImage>
class ITK_EXPORT StreamingGraphToImageFilter : public itk::ImageSource<TLabelImage>
{
public:
  /** Standard class typedefs. */
  typedef StreamingGraphToImageFilter                   Self;
  typedef itk::ImageSource<TLabelImage>                 Superclass;
  typedef itk::SmartPointer<Self>                       Pointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(StreamingGraphToImageFilter, ImageSource);

  /** Typedefs for processing */
  typedef typename TLabelImage::RegionType              RegionType;
  typedef typename TLabelImage::IndexType               IndexType;
  typedef typename TLabelImage::SizeType                SizeType;
  typedef typename TLabelImage::SpacingType             SpacingType;
  typedef typename TLabelImage::PointType               PointType;
  typedef typename TGraph::NodePointerType              NodePointerType;
  typedef itk::GrayscaleFillholeImageFilter<TLabelImage,TLabelImage> FillholeFilterType;
  typedef bg::model::point<float, 2, bg::cs::cartesian> point;
  typedef bg::model::box<point> box;
  typedef std::pair<box, unsigned> value;

  /** Set the input graph and build the R-Tree */
  void SetGraph(const TGraph graph);

  /** Prepare image allocation at the first call of the pipeline processing */
  virtual void GenerateOutputInformation(void);

  /** Does the real work. */
  virtual void GenerateData();

  itkSetMacro(OutputSize, SizeType);
  itkSetMacro(OutputOrigin, PointType);
  itkSetMacro(OutputSpacing, SpacingType);
  itkSetMacro(OutputProjectionRef, std::string);

protected:
  StreamingGraphToImageFilter(){};
  virtual ~StreamingGraphToImageFilter(){};

private:

  StreamingGraphToImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented

  TGraph       m_Graph;
  bgi::rtree< value, bgi::quadratic<16> > rtree;
  SizeType     m_OutputSize;
  SpacingType  m_OutputSpacing;
  PointType    m_OutputOrigin;
  std::string  m_OutputProjectionRef;
};

} /* namespace lsgrm */

#ifndef OTB_MANUAL_INSTANTIATION
#include <otbStreamingGraphToImageFilter.txx>
#endif

#endif /* MODULES_REMOTE_LSGRM_INCLUDE_OTBSTREAMINGGRAPHTOIMAGEFILTER_H_ */
