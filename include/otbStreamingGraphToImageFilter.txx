/*
 * otbStreamingGraphToImageFilter.txx
 *
 *  Created on: 6 nov. 2017
 *      Author: cresson
 */

#ifndef MODULES_REMOTE_LSGRM_INCLUDE_OTBSTREAMINGGRAPHTOIMAGEFILTER_TXX_
#define MODULES_REMOTE_LSGRM_INCLUDE_OTBSTREAMINGGRAPHTOIMAGEFILTER_TXX_

#include <otbStreamingGraphToImageFilter.h>

namespace lsgrm
{

template <class TGraph, class TLabelImage>
void
StreamingGraphToImageFilter<TGraph, TLabelImage>
::SetGraph(const TGraph graph)
 {
  m_Graph = graph;

  // Build a R-Tree
  itkDebugMacro( << "Building R-Tree" );

  rtree.clear();
  unsigned int count = 0;
  for(auto& node : m_Graph.m_Nodes)
    {
      // create a box
      box b(
          point(
              node->m_Bbox.m_UX,
              node->m_Bbox.m_UY),
          point(
              node->m_Bbox.m_UX + node->m_Bbox.m_W,
              node->m_Bbox.m_UY + node->m_Bbox.m_H));

      // insert new value
      rtree.insert(std::make_pair(b, count));
      count++;
    }

  itkDebugMacro( << "Building R-Tree finished" );

 }

template <class TGraph, class TLabelImage>
void
StreamingGraphToImageFilter<TGraph, TLabelImage>
::GenerateOutputInformation()
 {
  itkDebugMacro( << "Entering GenerateOutputInformation()" );

  // Output Largest Possible Region
  IndexType index;
  index.Fill(0);
  RegionType outputRegion(index, m_OutputSize);

  // Set output informations
  TLabelImage * outputPtr = this->GetOutput();
  outputPtr->SetOrigin ( m_OutputOrigin );
  outputPtr->SetSignedSpacing ( m_OutputSpacing );
  outputPtr->SetLargestPossibleRegion( outputRegion );
  outputPtr->SetProjectionRef(m_OutputProjectionRef);

 }


template <class TGraph, class TLabelImage>
void
StreamingGraphToImageFilter<TGraph, TLabelImage>
::GenerateData()
 {
  itkDebugMacro( << "Entering GenerateData()" );

  // Allocate the output buffer
  TLabelImage * outputPtr = this->GetOutput();
  RegionType outReqRegion = outputPtr->GetRequestedRegion();
  outputPtr->SetBufferedRegion(outputPtr->GetRequestedRegion());
  outputPtr->Allocate();

  // Find nodes intersecting find the output requested region
  box query_box(
      point(
          outReqRegion.GetIndex(0),
          outReqRegion.GetIndex(1)),
      point(
          outReqRegion.GetIndex(0)+outReqRegion.GetSize(0),
          outReqRegion.GetIndex(1)+outReqRegion.GetSize(1)));
  std::vector<value> result_s;
  itkDebugMacro( << "R-Tree query on output requested region " << outReqRegion );
  rtree.query(bgi::intersects(query_box), std::back_inserter(result_s));
  itkDebugMacro( << "R-Tree query done. Number of nodes: " << result_s.size() );

  // Retrieve the bounding box of the intersecting nodes (a kind of "Input requested region")
  box realBBox(query_box);
  for(auto& res : result_s)
    {
      boost::geometry::expand(realBBox, res.first);
    }
  IndexType index;
  index[0] = realBBox.min_corner().get<0>();
  index[1] = realBBox.min_corner().get<1>();
  SizeType size;
  size[0] = realBBox.max_corner().get<0>() - realBBox.min_corner().get<0>();
  size[1] = realBBox.max_corner().get<1>() - realBBox.min_corner().get<1>();
  RegionType inputRequestedRegion(index, size);
  itkDebugMacro( << "Input Requested region: " << inputRequestedRegion );

  // Generate the label image
  itkDebugMacro( << "Allocate buffered region ");
  const typename TLabelImage::InternalPixelType noDataLabel = 0;
  typename TLabelImage::Pointer labelImage = TLabelImage::New();
  labelImage->SetRegions(inputRequestedRegion);
  labelImage->Allocate();
  labelImage->FillBuffer(noDataLabel);
  itkDebugMacro( << "Allocate buffered region ok" );

  using LabelImageIterator = itk::ImageRegionIterator<TLabelImage>;
  LabelImageIterator it(labelImage, inputRequestedRegion);

  // Burn boundaries
  itkDebugMacro( << "Burning boundaries " );
  for(auto& res : result_s)
    {
      NodePointerType node = m_Graph.m_Nodes[res.second];

      lp::CellLists borderPixels;
      lp::ContourOperations::GenerateBorderCells(borderPixels, node->m_Contour, node->m_Id, m_OutputSize[0]);

      for (auto& pix: borderPixels)
        {
		  // patch raf (catch exception if position of pixel to set is not correct)
		  try {
          index[0] = (unsigned int)(pix % (std::size_t)(m_OutputSize[0]));
          index[1] = (unsigned int)(pix / (std::size_t)(m_OutputSize[0]));
          labelImage->SetPixel(index, res.second + 1);
		  } catch (std::exception e) {
			  std::cout << "Pixel ID: " << pix << std::endl;
			  std::cout << "Derived index: " << index[0] << "," << index[1] << std::endl;
			  std::cout << "Node position in list: " << res.second << std::endl;
		  }
		  // end patch
        }
    }
  itkDebugMacro( << "Burning boundaries ok ");

  // Fill holes
  itkDebugMacro( << "fill Hole filter" );
  typename FillholeFilterType::Pointer fillFilter = FillholeFilterType::New();
  fillFilter->SetInput(labelImage);
  fillFilter->Update();
  itkDebugMacro( << "Fill Hole filter OutputLargestPossibleRegion:" << fillFilter->GetOutput()->GetLargestPossibleRegion() );
  itkDebugMacro( << "fill Hole filter ok" );

  // Extract the stable region
  LabelImageIterator outIt(outputPtr, outReqRegion);
  LabelImageIterator inIt (fillFilter->GetOutput(), outReqRegion);
  for (inIt.GoToBegin(), outIt.GoToBegin(); !outIt.IsAtEnd(); ++outIt, ++inIt)
    outIt.Set(inIt.Get());
 }



} /* namespace lsgrm */

#endif
