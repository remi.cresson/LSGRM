#Contact: Pierre Lassalle or Remi Cresson
otb_fetch_module(LSGRM
  "Large scale generic region merging"
  GIT_REPOSITORY http://tully.ups-tlse.fr/cressonr/lsgrm.git
  GIT_TAG e2716d53c4eeb7eec1d3eb7ed680d345e5a6edcf
)
