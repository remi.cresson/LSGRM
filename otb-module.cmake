set(DOCUMENTATION "Large Scale Generic Region Merging")

otb_module(LSGRM
  DEPENDS
    otbGRM
    OTBCommon
    OTBApplicationEngine
    OTBConversion
  OPTIONAL_DEPENDS
    OTBMPI
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "Image segmentation"
)
